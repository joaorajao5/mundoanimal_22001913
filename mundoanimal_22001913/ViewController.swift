//
//  ViewController.swift
//  mundoanimal_22001913
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Alamofire
import Kingfisher

class ViewController: UIViewController {
    
    struct Animal: Decodable {
        let name : String
        let latin_name: String
        let image_link: String
    }

    @IBOutlet weak var imagemAnimal: UIImageView!
    @IBOutlet weak var nomeAnimal: UILabel!
    @IBOutlet weak var nomeLatim: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        let urlSession = URLSession.shared
        if let url = URL(string:"https://zoo-animal-api.herokuapp.com/animals/rand"){ urlSession.dataTask(with: url ){ data ,response , error in
            
            if let data = data{ print(data) };
            
            if let error = error{ print(error) }
            
        }.resume()
        
        }
        func retornarAnimal(){
            AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self){
                response in
                if let animal = response.value{
                    print(animal.image_link)
                }
            }
        }
        retornarAnimal()
    }


}

